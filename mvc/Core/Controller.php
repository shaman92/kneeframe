<?php

namespace Core;

/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 23.09.15
 * Time: 20:41
 */
class Controller
{
    public $model;
    public $twig;

    /**
     * Controller constructor.
     */
    public function __construct($modelObj = null, $twig = null) {
        $this->model = $modelObj;
        $this->twig = $twig;
    }
}