<?php
/**
 * Created by PhpStorm.
 * User: abakan
 * Date: 06.11.15
 * Time: 21:48
 */

namespace Core;


class Model {

    public $db;

    /**
     * Model constructor.
     */
    public function __construct($dbConnect) {
        $this->db = $dbConnect;
    }
}