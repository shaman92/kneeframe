<?php
/**
 * Created by PhpStorm.
 * User: abakan
 * Date: 06.11.15
 * Time: 21:11
 */

namespace Core;


use Controllers\mainController;
use Controllers\visitorController;
use Controllers\bannerController;
use Models\bannerModel;
use Pimple\Container;

class Application {

    public $container;

    /**
     * Application constructor.
     */
    public function __construct() {
        $this->container = new Container();
    }

    //@todo: Можно запихнуть имена классов в YML а потом просто проходить циклом и сетить лямбду)))
    //но пока так
    private function diContainer() {
        $this->container['db_host'] = self::getConfig('db_host');
        $this->container['db_name'] = self::getConfig('db_name');

        $this->container['twig'] = function ($c) {
            \Twig_Autoloader::register();
            $loader = new \Twig_Loader_Filesystem('../mvc/Views');

            return new \Twig_Environment($loader, [
                'cache'       => '../compilation_cache',
                'auto_reload' => true
            ]);
        };
        $this->container['db'] = function ($c) {
            return new Mongo($c['db_host'], $c['db_name']);
        };
        $this->container['mainCtrl'] = function ($c) {
            return new mainController(null, $c['twig']);
        };
        $this->container['bannerModel'] = function ($c) {
            return new bannerModel($c['db']->db);
        };
        $this->container['bannerCtrl'] = function ($c) {
            return new bannerController($c['bannerModel'], $c['twig']);
        };
    }

    private function router() {
        return \FastRoute\simpleDispatcher(function(\FastRoute\RouteCollector $r) {
            $r->addRoute('GET', '/', "mainCtrl@index");
            $r->addRoute('GET', '/banner/add', "bannerCtrl@showEditForm");
            $r->addRoute('POST', '/banner/save', "bannerCtrl@saveBanner");
        });
    }

    public function run() {
        $dispatcher = $this->router();
        $this->diContainer();

        // Fetch method and URI from somewhere
        $httpMethod = $_SERVER['REQUEST_METHOD'];
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $routeInfo = $dispatcher->dispatch($httpMethod, $uri);

        switch ($routeInfo[0]) {
            case \FastRoute\Dispatcher::NOT_FOUND:
                echo '<br>not found22';
                // ... 404 Not Found
                break;
            case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
                $allowedMethods = $routeInfo[1];
                // ... 405 Method Not Allowed
                echo 'method not allowed';
                break;
            case \FastRoute\Dispatcher::FOUND:
                $handler = explode('@', $routeInfo[1]);
                $this->container[$handler[0]]->$handler[1]();  //Тут объект контроллера берем уже из Пимпла!
                break;
        }
    }

    //просто получаю конфиг
    public static function getConfig($key) {
        $settings = parse_ini_file("../config/app.ini");

        if(isset($settings[$key])) {
            return $settings[$key];
        } else {
            return false;
        }
    }
}