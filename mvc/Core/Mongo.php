<?php
/**
 * Created by PhpStorm.
 * User: abakan
 * Date: 06.11.15
 * Time: 21:32
 */

namespace Core;


class Mongo {

    public $db;

    /**
     * Mongo constructor.
     * @param $host
     * @param $dbName
     * @internal param $db
     */
    public function __construct($host, $dbName) {
        $connect = new \MongoClient($host);
        $this->db = $connect->$dbName;
    }

}