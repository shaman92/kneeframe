<?php

namespace Models;


use Core\Model;

class bannerModel extends Model {
    public function insert($document) {
        $result = $this->db->banners->insert($document);

        if(!empty($result['err'])) {
            echo 'error';
        } elseif(!empty($result['ok'])) {
            return true;
        }
    }


    public function findById($id) {
        $mongoId = new \MongoId($id);
        $result = $this->db->banners->findOne(['_id' => $mongoId]);
        return $result ? $result : false;
    }

    public function findBySiteId($site_id) {
        $mongoId = new \MongoId($site_id);
        $result = $this->db->banners->find(['sites' => ['$in'=>[$mongoId]] ]);
        return $result ? $result : false;
    }

}