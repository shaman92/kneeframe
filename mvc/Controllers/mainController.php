<?php

namespace Controllers;

use Models\siteModel;
use Core\App;
use Core\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 23.09.15
 * Time: 20:41
 */
class mainController extends Controller {

    public function index() {
        $layout = $this->twig->loadTemplate('layout.html');
        $this->twig->display('main.html', ['layout' => $layout]);
    }

}