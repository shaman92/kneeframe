<?php

namespace Controllers;

use Core\App;
use Core\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 23.09.15
 * Time: 20:41
 */
class bannerController extends Controller {

    public function showEditForm() {
        $layout = $this->twig->loadTemplate('layout.html');
        $this->twig->display('admin/banner_add.html', ['layout' => $layout, 'test'=>'ololo']);
    }

    public function saveBanner() {
        $request = Request::createFromGlobals();
        $options = $request->request->all();

        $result = $this->model->insert($options);
        echo $result ? 'success' : null;

    }

}